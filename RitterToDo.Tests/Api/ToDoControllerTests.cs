﻿using System.Web.Mvc;
using FakeItEasy;
using Moo;
using NUnit.Framework;
using RitterToDo.Api;
using RitterToDo.Core;
using RitterToDo.Models;
using RitterToDo.Repos;
using System;
using RitterToDo.Tests.TestHelpers;
using Ploeh.AutoFixture;
using Should;
using System.Linq;
using System.Collections.Generic;

namespace RitterToDo.Tests.Api
{
    [TestFixture]
    public class ToDoControllerTests
    {
        private ToDoController CreateSUT()
        {
            return new ToDoController();
        }

        [Test]
        public void Detail_GetAll_PopulatesView()
        {
            // * Arrange
            //   - Preparing data and mocks
            var sut = CreateSUT();
            var fixture = new Fixture();
            var model = fixture.Create<ToDoDetailViewModel>();
            var entity = fixture.Create<ToDo>();
            var id = Guid.NewGuid();
            var mapperMock = A.Fake<IExtensibleMapper<ToDo, ToDoDetailViewModel>>();

            //   - Setting up expectations
            A.CallTo(() => sut.MappingRepository.ResolveMapper<ToDo, ToDoDetailViewModel>()).Returns(mapperMock);
            A.CallTo(() => sut.ToDoRepo.GetAll());
            A.CallTo(() => mapperMock.Map(entity)).Returns(model);

            // * Act
            var result = sut.Get();

            // * Assert
            var vr = result.ShouldNotBeNull();
        }
        [Test]
        public void Detail_GetById_PopulatesView()
        {
            // * Arrange
            //   - Preparing data and mocks
            var sut = CreateSUT();
            var fixture = new Fixture();
            var model = fixture.Create<ToDoDetailViewModel>();
            var entity = fixture.Create<ToDo>();
            var id = Guid.NewGuid();
            var mapperMock = A.Fake<IExtensibleMapper<ToDo, ToDoDetailViewModel>>();

            //   - Setting up expectations
            A.CallTo(() => sut.MappingRepository.ResolveMapper<ToDo, ToDoDetailViewModel>()).Returns(mapperMock);
            A.CallTo(() => sut.ToDoRepo.GetById(id)).Returns(entity);
            A.CallTo(() => mapperMock.Map(entity)).Returns(model);

            // * Act
            var result = sut.Get(id);

            // * Assert
            var vr = result.ShouldNotBeNull();
        }

    }
}
