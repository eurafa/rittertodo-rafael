﻿using Moo;
using RitterToDo.Core;
using RitterToDo.Models;
using RitterToDo.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Web.Http;

namespace RitterToDo.Api
{
    public class ToDoController : ApiController
    {

        public IRepository<ToDo> ToDoRepo = new ToDoRepository(new IdentityHelper(), new ApplicationDbContext());

        public IMappingRepository MappingRepository = new MappingRepository();
        
        // GET api/<controller>
        public IEnumerable<ToDoViewModel> Get()
        {

            var entities = ToDoRepo.GetAll().Where(x => x.OwnerId == "320840b0-78d6-4aed-9912-65c1cd180990");
            if (entities == null)
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }
            var mapper = MappingRepository.ResolveMapper<ToDo, ToDoViewModel>();
            var models = mapper.MapMultiple(entities);
            return models;
        }

        // GET api/<controller>/5
        public ToDoViewModel Get(Guid id)
        {
            var entity = ToDoRepo.GetById(id);
            if (entity == null || !entity.OwnerId.Equals("320840b0-78d6-4aed-9912-65c1cd180990"))
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

            var mapper = MappingRepository.ResolveMapper<ToDo, ToDoViewModel>();
            var model = mapper.Map(entity);
            return model;
        }

        /*
        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
         * */
    }
}